import express from 'express'

const app = express();

app.use(express.static('public'));

app.listen(8080, () => {
    console.log("El servidor se está ejecutando en http://localhost:8080");
});